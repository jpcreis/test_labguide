package circuits;

import java.lang.reflect.Array;

public abstract class LogicGate {
	
	private LogicVariable[] in = new LogicVariable[2];
	private LogicVariable out;
	
	public LogicGate(LogicVariable w1, LogicVariable x1, LogicVariable x2) throws ColisionException{
		this.setInputs(new LogicVariable[]{x1,x2});
		this.setOutput(w1);
		w1.setLogicGate(this);
	}
	
	public LogicGate(LogicVariable w1, LogicVariable x1) throws ColisionException{
		this.setInputs(new LogicVariable[]{x1});
		this.setOutput(w1);
		w1.setLogicGate(this);
	}

	public LogicVariable getOutput() {
		// TODO Auto-generated method stub
		return this.out;
	}

	public LogicVariable[] getInputs() {
		// TODO Auto-generated method stub
		return this.in;
	}
	
	public void setOutput(LogicVariable o) {
		this.out = o;
	}
	
	public void setInputs(LogicVariable[] i) {
		this.in = i;
	}

	public abstract String getSymbol();
	public abstract String getFormula(); 

}
