package circuits;

import java.util.ArrayList;

public class CombinatorialCircuit {

	private ArrayList<LogicVariable> variables = new ArrayList<LogicVariable>();
	
	public CombinatorialCircuit() {
		
	}
			
	public boolean addVariable(LogicVariable a) {
		if (getVariableByName(a.getName()) == null) {
			this.variables.add(a);
			return true;
		}else {
			return false;
		}
	}

	public LogicVariable getVariableByName(String string) {
		// TODO Auto-generated method stub
		for (LogicVariable v : this.variables) {
			if (v.getName().compareTo(string) == 0){
				return v;
			}
		}
		return null;
	}

	 
	
}
