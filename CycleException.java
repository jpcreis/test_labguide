package circuits;

public class CycleException extends Exception {

	public CycleException(String message) {
        super(message);
        System.out.println(message);
    }
	
}
