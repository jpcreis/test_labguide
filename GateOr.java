package circuits;

public class GateOr extends LogicGate {

	public GateOr(LogicVariable w1, LogicVariable x1, LogicVariable x2) throws ColisionException, CycleException {
		
		super(w1,x1,x2);
	
	}
	
	public String getSymbol() {
		// TODO Auto-generated method stub
		return "OR";
	}
	
	public String getFormula() {
		
		String s = new String();
		
		s += getSymbol();
		s += "(";
		s += this.getInputs()[0].getFormula();
		s += ",";
		s += this.getInputs()[1].getFormula();
		s += ")";
		
		return s;
	}
	
}
