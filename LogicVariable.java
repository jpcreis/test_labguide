package circuits;

public class LogicVariable{

	private String name;
	private boolean value; 
	private LogicGate lg = null;
	
	public void setLogicGate(LogicGate g) throws ColisionException {
		
		if (this.lg == null) {
			this.lg = g;
		}else {
			throw new ColisionException("Not possible!");
		}
	}
	
	public LogicGate getLogicGate() {
		return this.lg;
	}
	
	public LogicVariable(String string, boolean b) {
		this.name = string;
		this.value = b;
	}
	
	public LogicVariable(String string) {
		this.name = string;
	}

	public String getName() {
		// TODO Auto-generated method stub
		return this.name;
	}

	public boolean getValue() {
		// TODO Auto-generated method stub
		if (lg != null) {
			if (lg.getSymbol().compareTo("NOT") == 0) {
				this.value = !lg.getInputs()[0].getValue();
			}else if (lg.getSymbol().compareTo("AND") == 0) {
				this.value = lg.getInputs()[0].getValue() && lg.getInputs()[1].getValue();
			}else if (lg.getSymbol().compareTo("OR") == 0) {
				this.value = lg.getInputs()[0].getValue() || lg.getInputs()[1].getValue();
			}
		}
		
		return this.value;
	}
	
	public void setValue(boolean b) {
		this.value = b;
		
	}
	
	public boolean equals(Object b) {
	
		LogicVariable temp = (LogicVariable)b;
		
		if (this.name.compareTo(temp.getName()) == 0) {
			return true;
		}
		return false;
	}

	public LogicGate getCalculatedBy() {
		// TODO Auto-generated method stub
		return lg;
	}

	public String getFormula() {
		// TODO Auto-generated method stub
		if (lg == null) {
			return getName();
		}else {
			return lg.getFormula();
		}
	}

	
	
}
