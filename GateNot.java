package circuits;

public class GateNot extends LogicGate {

	public GateNot(LogicVariable w1, LogicVariable x1) throws ColisionException, CycleException {
		
		super(w1,x1);
		
		if (x1.getLogicGate() != null) {			
			if(w1.equals(x1.getLogicGate().getInputs()[0]) || w1.equals(x1.getLogicGate().getInputs()[1])) {
				
				throw new CycleException("Not possible cycle!");
			}
		}
		
	}
	
	public String getSymbol() {
		// TODO Auto-generated method stub
		return "NOT";
	}
	
public String getFormula() {
		
		String s = new String();
		
		s += getSymbol();
		s += "(";
		s += this.getInputs()[0].getFormula();
		s += ")";
		
		return s;
	}
	
}
